package com.zuitt.example;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = userInput.nextLine();

        System.out.println("Last Name:");
        String lastName = userInput.nextLine();

        System.out.println("First Subject Grade:");
        double firstGrade = new Double(userInput.nextLine());

        System.out.println("Second Subject Grade:");
        double secondGrade = new Double(userInput.nextLine());

        System.out.println("Third Subject Grade:");
        double thirdGrade = new Double(userInput.nextLine());

        double average = (firstGrade + secondGrade + thirdGrade)/3;

        System.out.println("Good Day, " + firstName + " " +lastName);
        System.out.println("Your grade average is: " + average);
    }
}
