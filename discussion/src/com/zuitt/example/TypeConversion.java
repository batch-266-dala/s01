package com.zuitt.example;

import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args) {

//        Scanner age = new Scanner(System.in);
//        System.out.println("How old are you?");
//        String userAge = age.nextLine();
//        System.out.println("The age is " + userAge);
        Scanner userAge = new Scanner(System.in);
        System.out.println("How old are you?");
        double age = new Double(userAge.nextLine());

        System.out.println("This is a confirmation that you are " + age + " years old");
    }
}
