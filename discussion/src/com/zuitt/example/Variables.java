package com.zuitt.example;

public class Variables {
    /* Data types
    Primitive
    * - int for integer
    * - double for float
    * - char for single characters
    * - boolean for boolean values
    Non-Primitive Data types
    - String
    - Arrays
    - Classes
    - Interface
    * */
    public static void main(String[] args) {
        int age = 18;
        char middleInitial = 'V';
        boolean isLegalAge = true;

        System.out.println("The user age is " + age);
        System.out.println("The user middle initial is " + middleInitial);
        System.out.println("Is the user of legal age? " + isLegalAge);
    }



}
